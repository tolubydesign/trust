
# Documentation 

## Short Description


## Running Project

To run the project, you will need Docker and Docker Compose installed onto your machine. Docker Compose is what I have used to handle the PostgreSQL database.
I have provided a link as to how to install docker and docker compose below. Please do not hesitate to message me if there are any issues

[Install Docker Compose](https://docs.docker.com/compose/install/)

[Install Docker](https://docs.docker.com/engine/install/)

Terminal Commands to Run the `front-end`
```sh
1 cd front-end
2 npm i 
3 npm run dev
```
This will open up the NextJS Web Application on `http://localhost:3000`

Terminal commands to run the `back-end`

```sh
1 cd back-end
2 docker compose up
3 npm i
4 npm run serve
```
Doing this will open the port 4000. The graphql api can be accessed on `http://localhost:4000/graphql`
I have left further instructions as to how one can populate the database within the back-end README.md file.

## Use of Tech Stack

This project was built using NextJS, for the front end, and Apollo GraphQL for the back end. The front end was created using NextJS due to its connection to the react library and ease of use. NextJS also supports Server Side Rendering and Static Site Generation, allowing for more options when it comes to deploying an application. It is important to note, that NextJS is not the only library to offer this option.
The back end was created with Apollo GraphQL using Typescript and Express. GraphQL allows developers to create fairly stable and maintainable servers. It's fun to use too.

Personally, given additional time I would have preferred creating a back-end that utilised Golang. Golang beats NodeJS in performance and scalability. Due to time constraints and unforeseen variables, I opted to develop with Node. While it's a little hard to pick up and learn it provides an enjoyable development experience.

I have provided a link to an example Golang REST API project I have been working on below.

[Golang Back-end REST API](https://github.com/tolubydesign/angular-story-backend)

### Why use NextJS for the front-end 
  + Easy to install.
  + Allows a developer to just right into code without having to think about how the site will be compiled
  + Built on Rust.
  + Works similarly to the React library.
  + Can be used to build server-side rendered (SSR) and static site 

A more in-depth list of why one would use NextJS can be found here:\
[What is Next.js?](https://nextjs.org/learn/foundations/about-nextjs/what-is-nextjs)\
[Why Next.js 13 is a Game-Changer](https://blog.bitsrc.io/why-next-js-13-is-a-game-changer-2167658d9de2)

### Why use Apollo Graphql
  + Its declarative.
  + Ordered and consistent response from the sever.
  + Structured in a way that newer development members can easily jump onto the project.

I have provided additional links discussing the benefits and downsides of using Apollo GraphQL:\
[9 Ways To Secure your GraphQL API — GraphQL Security Checklist](https://www.apollographql.com/blog/graphql/security/9-ways-to-secure-your-graphql-api-security-checklist/)\
[GraphQL API Security: 5-things developers should know](https://www.computerweekly.com/blog/CW-Developer-Network/GraphQL-API-Security-5-things-developers-should-know)\
[What is GraphQL? Pros and Cons, Top Security Issues, and Mitigation Strategies](https://www.wevolver.com/article/what-is-graphql-pros-and-cons-top-security-issues-and-mitigation-strategies)\
[Advantages and Disadvantages of GraphQL](https://stablekernel.com/article/advantages-and-disadvantages-of-graphql/)



