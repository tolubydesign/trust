"use client";
import HomePageBanner from '@/components/banner/home-page-banner'
import CaseStudiesCarousel from '@/components/sections/case-studies-carousel'
import TrustedByBrands from '@/components/sections/trusted-by-brands'
import WhatWeDo from '@/components/sections/what-we-do'
import Image from 'next/image'

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center">
      <HomePageBanner />
      <WhatWeDo />
      <CaseStudiesCarousel />
      <TrustedByBrands />
    </main>
  )
}


