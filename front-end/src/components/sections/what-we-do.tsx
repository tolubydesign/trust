import Image from 'next/image';
import navClass from "@/styles/modules/what-we-do.module.scss";
import Link from 'next/link';
import TextHeader from '@/components/header/section-header';
import BasicSectionHeader from '@/components/header/basic-section-header';


type ImageReason = {
  title: string,
  description: string,
  url: string,
  width: number,
  height: number,
}

export default function WhatWeDo() {
  const sectionClassName = 'flex flex-col justify-center items-center mx-auto w-full px-[30px] md:px-[80px] mt-[44px] md:mt-[84px]';

  const reasons: ImageReason[] = [
    {
      title: "Web development",
      description: "We use cutting-edge web development technologies to help our clients fulfill their business goals through functional, reliable solutions.",
      url: "/what-we-do/Web Dev Icon.svg",
      width: 64,
      height: 64
    },
    {
      title: "User experience & design",
      description: "Our complete web design services will bring your ideas to life and provide you with a sleek, high-performing product that elevates your business.",
      url: "/what-we-do/UX Icon.svg",
      width: 64,
      height: 64
    },
    {
      title: "Mobile app development",
      description: "Our extensive mobile development experience allows us to create custom native and cross-platform iOS and Android mobile solutions for our clients.",
      url: "/what-we-do/App Dev Icon.svg",
      width: 64,
      height: 64
    },
    {
      title: "Blockchain solutions",
      description: "We conduct market research to determine the optimal blockchain-based solutions to help you grow your company and achieve your business goals.",
      url: "/what-we-do/block-chain-icon.svg",
      width: 64,
      height: 64
    },
  ]

  return (
    <div className={sectionClassName}>
      <div className="max-w-[1280px]">
        <div className="flex justify-center w-full mb-[26px] md:mb-[46px]">
          <TextHeader header="What we do" />
        </div>
        <BasicSectionHeader header="We offer a complete range of bespoke design and development services to help you turn your ideas into digital masterpieces" />

        <div className='grid gap-[20px] md:gap-[40px] sm:grid-cols-2 md:grid-cols-4 mt-[44px] md:mt-[54px] mx-auto'>
          {
            reasons.map((reason, index) => <ReasonWeDo key={`${reason.title} ${index}`}
              description={reason.description}
              height={reason.height}
              width={reason.width}
              url={reason.url}
              title={reason.title} />)
          }
        </div>
      </div>
    </div>
  )
};


// Create a smaller function to handle repeatable code that is only used within a specific area. 
// On a larger project, if this repeats, a common reusable function would be needed.
const ReasonWeDo = ({ description, height, title, url, width }: ImageReason) => {
  return (
    <div className='max-w-[290px]'>
      <div className='mb-[20px] sm:mb-[40px] md:mb-[53px]'>
        <Image
          src={url}
          alt={`${title} icon.`}
          className="max-w-[44px] sm:max-w-[54px] md:max-w-[64px]"
          width={width}
          height={height}
        />
      </div>

      <div className='min-h-[30px] sm:min-h-[60px] md:min-h-[110px]'>
        <h4 className='text-[18px] md:text-[24px] font-bold'>
          {title}
        </h4>
      </div>

      <div className=''>
        <p className='text-[16px] md:text-[20px] font-normal'>
          {description}
        </p>
      </div>


    </div>
  )
}