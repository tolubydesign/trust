import Image from 'next/image';
import TextHeader from '@/components/header/section-header';
import BasicSectionHeader from '@/components/header/basic-section-header';
import { useEffect, useState } from 'react';

type Brand = {
  id: string,
  name: string,
  alt: string,
  url: string
}

/**
 * Make Graphql request to fetch brands.
 * @returns Brands
 */
const getAllBrands = async (): Promise<any> => {
  const query = `query getAllBrands {
    getAllBrands {
      id
      name
      alt
      url
    }
  }`;
  const res = await fetch(
    `http://localhost:4000/graphql`,
    {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        query: query
      }),
    }
  )

  const data = await res.json();
  if (!data || data.data.errors) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error('Failed to fetch data')
  }

  return data.data.getAllBrands
};

/**
 * Make Graphql request to fetch brands in ascending order.
 * @returns Brands
 */
const getAllBrandAsc = async (): Promise<any> => {
  const query = `query getAllBrandAsc {
    getAllBrandAsc {
      id
      name
      alt
      url
    }
  }`;
  const res = await fetch(
    `http://localhost:4000/graphql`,
    {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        query: query
      }),
    }
  )

  const data = await res.json();
  if (!data || data.data.errors) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error('Failed to fetch data')
  }

  return data.data.getAllBrandAsc
};

/**
 * Make Graphql request to fetch brands in descending order.
 * @returns Brands 
 */
const getAllBrandDesc = async (): Promise<any> => {
  const query = `query getAllBrandDesc {
    getAllBrandDesc {
      id
      name
      alt
      url
    }
  }`;
  const res = await fetch(
    `http://localhost:4000/graphql`,
    {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        query: query
      }),
    }
  )

  const data = await res.json();
  if (!data || data.data.errors) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error('Failed to fetch data')
  }

  return data.data.getAllBrandDesc
};


export default async function TrustedByBrands() {
  const sectionClassName = 'flex flex-col justify-center items-center mx-auto w-full px-[30px] md:px-[80px]';
  const sortingButtonClassName = `rounded-full bg-[#70259B] text-white font-medium text-[14px] md:text-[16px] px-[13px] py-[6px] mx-[6px]`;
  const [sortedBrand, setSortedBrands] = useState<Brand[]>([]);
  let brands = await getAllBrands();

  const fetchBrandsByAsc = async () => {
    brands = await getAllBrandAsc();
  }

  const fetchBrandsByDesc = async () => {
    brands = await getAllBrandDesc();
  }

  return (
    <div className={sectionClassName}>
      <div className="max-w-[1280px] w-full">
        <div className="flex justify-center w-full mb-[26px] md:mb-[46px]">
          <TextHeader header="You’ll be in good company" />
        </div>
        <BasicSectionHeader header="Trusted by leading brands" />

        <div>
          <button onClick={() => fetchBrandsByAsc()} className={sortingButtonClassName}>Sort in Ascending Order</button>
          <button onClick={() => fetchBrandsByDesc()} className={sortingButtonClassName}>Sort in Descending Order</button>
        </div>

        <div className='grid gap-1 grid-cols-5 mt-[80px]'>
          {
            brands ?
              brands.map((brand: Brand, index: number) => <Image
                key={`brand-${brand}-${index}`}
                alt={brand.alt}
                width={256}
                height={120}
                src={brand.url} />)
              : ""
          }
        </div>

      </div>
    </div>
  )
};

