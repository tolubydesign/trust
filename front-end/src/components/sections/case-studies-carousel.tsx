"use client"
import Carousel, { ResponsiveType } from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import TextHeader from '@/components/header/section-header';
import Image from 'next/image';
import carouselClass from "@/styles/modules/case-studies-carousel.module.scss";

type CarouselSlideProps = {
  src: string,
  title: string,
  description: string,
}

export default function CaseStudiesCarousel() {
  const sectionClassName = `flex flex-col justify-center items-center mx-auto w-full py-[44px] md:py-[84px] px-[30px] md:px-[80px]`;

  const responsive: ResponsiveType = {
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3,
    },
    tablet: {
      breakpoint: { max: 950, min: 464 },
      items: 2,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  };

  const carouselContent: CarouselSlideProps[] = [
    {
      src: "/the-olympian1280x960.png",
      title: "The Olympian",
      description: "The only athlete in the world to do her Olympic routine in 2020.",
    },
    {
      src: "/the-saving-jar-1280x960.png",
      title: "The Savings Jar",
      description: "Grow your savings treasure and grow your dragon.",
    },
    {
      src: "/skhokho-semali-1280x960.png",
      title: "Skhokho seMali",
      description: "Helping South Africans become #CashCleva with Skhokho and TymeBank.",
    },

  ]

  return (
    <div className={sectionClassName}>
      <div className="max-w-[1280px] w-full">
        <div className="flex flex-col justify-center w-full mb-[26px] md:mb-[46px]">
          <TextHeader header="Case studies" />

          <div className='mt-[46px] md:mt-[57px]'>
            <Carousel
              responsive={responsive}
              infinite={true}
              arrows={false}
              showDots={true}
              swipeable={true}
              draggable={true}
              autoPlaySpeed={1000}
              containerClass="justify-between"
              itemClass="min-h-[400px] pr-[30px]" renderButtonGroupOutside={true}>
              {carouselContent.map((content, index) => <CarouselSlide
                key={`${content.src}-${index}`}
                src={content.src}
                title={content.title}
                description={content.description} />)}
            </Carousel>
          </div>
        </div>
      </div>
    </div>
  )
}

const CarouselSlide = ({ src, title, description }: CarouselSlideProps) => {
  const lineClassName = `${carouselClass.carouselSlideLineGroup}`;
  const imageGroupClassName = `absolute -z-10 w-full h-full md:min-h-[400px] overflow-hidden`;
  const linearGradient = `${carouselClass.linearGradientOverlay}`

  return (
    <div className='flex relative w-full h-full md:max-w-[430px] md:max-h-[400px]'>
      <div className={imageGroupClassName}>
        <Image
          className='w-[130%] h-auto min-w-[400px]'
          quality={100} src={src}
          width={400}
          height={400}
          alt={`Static background for ${title}`} />
      </div>

      <div className={linearGradient}></div>

      <div className='mt-auto px-[16px] md:px-[24px] pb-[16px] md:pb-[24px]'>
        <div className={lineClassName}><hr /></div>

        <h3 className={'text-white text-[26px] md:text-[32px] font-bold'}>
          {title}
        </h3>
        <p className={'text-white text-[14px] md:text-[16px] font-normal'}>
          {description}
        </p>
      </div>

    </div>
  )
}