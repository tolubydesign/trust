import Image from 'next/image';
import navClass from "@/styles/modules/navigational-bar.module.scss";
import Link from 'next/link';

export default function NavigationalBar() {
  return (
    <div className={`${navClass.section} w-full`}>
      <div className={`${navClass.content} flex flex-row justify-between items-center px-[20px] py-[22px] mx-auto`}>

        <div className={navClass.logoGroup}>
          <Image
            src="/logo.svg"
            alt="Brand main Logo"
            className=""
            width={100}
            height={31}
            priority
          />
        </div>

        <div className={navClass.linkGroup}>
          <Link href={"/services"} className={navClass.link}>
            Services
          </Link>

          <Link href={"/industries"} className={navClass.link}>
            Industries
          </Link>

          <Link href={"/cases"} className={navClass.link}>
            Cases
          </Link>

          <Link href={"/contact"} className={navClass.link}>
            Contact
          </Link>
        </div>

        <div>
          <button className={navClass.letsTalkButton}>
            Let's Talk
          </button>
        </div>
      </div>
    </div>
  )
}