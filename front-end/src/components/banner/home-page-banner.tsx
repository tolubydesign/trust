import heroClass from "@/styles/modules/home-page-banner.module.scss";

export default function HomePageBanner() {
  const sectionClassName= `${heroClass.section} w-full flex px-[16px] py-[30px] md:px-[20px] md:py-[86px]`;
  const contentClassName = `${heroClass.content} px-[20px] md:px-[80px] flex flex-col justify-end items-start mx-w md:mx-w-[50%]`;
  const descriptionClassName = `text-white text-[] text-left`;
  const viewProjectButton = `${heroClass.projectButton} rounded-full bg-[#70259B] text-[14px] font-bold text-white px-[20px] py-[10px] mt-[30px] md:mt-[57px]`;

  return (
    <div className={sectionClassName}>
      <div className={contentClassName}>
        <h1 className={`text-[48px] text-left font-bold text-white`}>
          Live with Confidence
        </h1>
        <p className={descriptionClassName}>
          José Mourinho brings confidence to pan-African Sanlam campaign.
        </p>

        <button className={viewProjectButton}>
          View project
        </button>
      </div>
    </div>
  )
}