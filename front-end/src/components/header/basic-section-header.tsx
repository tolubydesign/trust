// Note. can become 
type TextHeaderProps = {
  header: string
}

export default function BasicSectionHeader({header}: TextHeaderProps) {
  const sectionClassName = ``;
  const headerClassName = `text-[38px] md:text-[48px] font-bold text-[#242424]`;
  return (
    <div className={sectionClassName}>
      <h3 className={headerClassName}>{header}</h3>
    </div>
  )
}