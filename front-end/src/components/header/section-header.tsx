import headerClass from "@/styles/modules/section-header.module.scss";

/**
 * Note. Creating as many reusable components as possible. 
 */

type TextHeaderProps = {
  header: string
}

export default function TextHeader({header}: TextHeaderProps) {
  const sectionClassName = `${headerClass.section} flex flex-row justify-center items-center mr-auto`;
  const headerClassName = `text-[24px] font-normal text-[#000]`;
  const lineClassName = `${headerClass.lineGroup} mr-[20px]`;
  return (
    <div className={sectionClassName}>
      <div className={lineClassName}><hr/></div>
      <h3 className={headerClassName}>{header}</h3>
    </div>
  )
}