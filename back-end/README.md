# Running the Apollo Graphql Server

## Basic Structure of Server

The back-end was built with GraphQL (in combination with Express) and PostgreSQL Database.

## Engage with database directly

If you wish to interact with the Postgres database directly, you can do so by typing.

Note. The Postgres database must be running to connect with it, via your terminal.
```sh
$ docker exec -it posgresqltrustcontainer psql -d postgres -U postgres
```

## Known issues

Run `docker compose up` in the terminal, to start up the database in a docker container should return a response similar to 
```sh 
[+] Running 1/0
 ✔ Container posgresqltrustcontainer  Created 0.0s 
Attaching to posgresqltrustcontainer
posgresqltrustcontainer  | 
posgresqltrustcontainer  | PostgreSQL Database directory appears to contain a database; Skipping initialization
posgresqltrustcontainer  | 
posgresqltrustcontainer  | 
posgresqltrustcontainer  | 2023-09-10 00:52:36.239 UTC [1] LOG:  starting PostgreSQL 15.2 (Debian 15.2-1.pgdg110+1) on x86_64-pc-linux-gnu, compiled by gcc (Debian 10.2.1-6) 10.2.1 20210110, 64-bit
posgresqltrustcontainer  | 2023-09-10 00:52:36.240 UTC [1] LOG:  listening on IPv4 address "0.0.0.0", port 5432
posgresqltrustcontainer  | 2023-09-10 00:52:36.240 UTC [1] LOG:  listening on IPv6 address "::", port 5432
posgresqltrustcontainer  | 2023-09-10 00:52:36.241 UTC [1] LOG:  listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
posgresqltrustcontainer  | 2023-09-10 00:52:36.247 UTC [29] LOG:  database system was shut down at 2023-09-10 00:50:51 UTC
posgresqltrustcontainer  | 2023-09-10 00:52:36.257 UTC [1] LOG:  database system is ready to accept connections
```

___

It is important to run the docker compose yml, containing the database, before running `npm run serve`

A successful npm run serve will look like this, in the terminal.

```sh
npm run serve

> back-end@1.0.0 serve
> nodemon NODE_ENV=development npm run start

[nodemon] 3.0.1
[nodemon] reading config ./nodemon.json
[nodemon] to restart at any time, enter `rs`
[nodemon] or send SIGHUP to 9835 to restart
[nodemon] ignoring: .git _postgresql_database/** ./dist/**/*
[nodemon] watching path(s): src/**/** src/server.ts src/shared/schema/**
[nodemon] watching extensions: js,mjs,cjs,json
[nodemon] starting `npm run compile && ts-node ./dist/server.js NODE_ENV=development npm run start`
[nodemon] spawning
[nodemon] child pid: 9847
[nodemon] watching 11 files

> back-end@1.0.0 compile
> tsc

Total connections:  0
Awaiting connections:  0
Idle connections:  0
Sever up. On port: http://localhost:4000/graphql
```

If this is not what you see shutdown the docker compose yml, by inputting `docker compose down` in the terminal, and shutdown the server in the terminal.
Now restart the process.

___

If you do not see something similar to this drop the container by inputting `docker compose down` into your terminal. Rerun the container via `docker container up` and the issues should be resolved.

___

On first load of the project, to simulate inputting data into the "database" for later consumption, you will need to initialise the PostgreSQL database. 
This can be accomplished by going to the graphql portal, `http://localhost:4000/graphql` clicking on the _Root_ tab, selecting _mutations_ and then _initialiseDatabase_.
Make sure to include the Fields `message` and `status`.

<div align="center"></div>
<img src="assets/Screenshot_20230910_031100.png" alt="drawing" width="1000"/>
</div>

___

## Improvements

If allowed more time I would have opted to use tools and services like Minio or AWS S3 to store and distribute files. If AWS S3 was the chosen service users could expect a quicker response time when trying to access files. This can be done via CloudFront. This does come at a cost. Doing this will result in an increased cost to business as AWS services are not cheap, at scale.

Additional work can be done to improve the security and sorting of brands. 

The Graphql server can be expanded to easily include logging and authentication as a means to protect the data on our database.

