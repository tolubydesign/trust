import fs from "fs";
import { QueryResult } from "pg";
import postgresqlConnection from "../../utils/connection/postgresql.connection.js";
import { createPostgreSQLConnection } from "../../helpers/database/checks.js";
import { Brand, HTTPResponse } from "./model.js";

/**
 * @description Mutation object to collect all back-end mutations functions.
 * Note: As a project grows these function can be placed in separate files and folder. Helping to maintain clean and understandable code.
 */
export const Mutation = {
  /**
   * Initialise the PostgreSQL with data that users can consume.
   */
  initialiseDatabase: async (): Promise<HTTPResponse> => {
    const fileList: string[] = fs.readdirSync("./public/brands");
    // Better typing can be added here.
    let foundServerBrands: any[string] = []
    fileList.forEach((filename: string) => {
      const name = filename.split('.').slice(0, -1).join('.')
      foundServerBrands.push([
        name,
        `${name.replace(/^\w/, (c) => c.toUpperCase())} logo.`,
        `http://localhost:4000/static/brands/${filename}`
      ])
    });

    const connection = await createPostgreSQLConnection(postgresqlConnection);
    // This a terrible execution. Work can be done here to improve performance and security.
    const sqlQuery = `INSERT INTO brand (name, alt, url) VALUES($1, $2, $3)`
    // Delete all current values in table.
    await connection.query("TRUNCATE brand");

    const promises = foundServerBrands.map(async (file: string[]) => {
      console.log('await await file', file);
      return await connection.query(sqlQuery, file); 
    })    
    await Promise.all(promises);

    // console.log("...foundServerBrands", [...foundServerBrands]);
    // console.log("totalNumberOfInputs", totalNumberOfInputs);


    // Alternative way of updating database with new brands
    // This method finds duplicate brands and only adds brand that dont already exist in the database
    // 
    // Check that files dont already exist in the database
    // const connection = await createPostgreSQLConnection(postgresqlConnection);
    // const brandsQueryResult: QueryResult<Brand> = await connection.query("SELECT * FROM brand");

    // const brandsOnDatabase: Brand[] = brandsQueryResult.rows;
    // // Find brands that aren't already in the database.
    // const uniqueValues = foundServerBrands.filter((brand: Brand) => {
    //   const onDatabase = brandsOnDatabase.find((b: Brand) => brand.name === b.name)
    //   console.log("already on database", onDatabase);
    //   if (!onDatabase) return brand;
    // })

    return {
      status: "OK",
      message: "Database was successfully populated.",
    };
  },
};