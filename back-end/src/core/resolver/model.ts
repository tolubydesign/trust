export type Brand = {
  id?: string,
  name: string,
  alt: string,
  url: string,
}

export type HTTPResponse = {
  status: string,
  message: string,
}