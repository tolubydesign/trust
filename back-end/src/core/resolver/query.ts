 import { GraphQLError } from 'graphql';
import { ServerResponse } from 'http';
import postgresqlConnection from './../../utils/connection/postgresql.connection.js';
import { createPostgreSQLConnection } from './../../helpers/database/checks.js';
import { Brand } from './model.js';
import { QueryResult } from 'pg';

/**
 * @description Create a query object to collect all back-end query functions.
 */
export const Query = {
  getAllBrands: async (): Promise<Brand[] | undefined> => {
    const connection = await createPostgreSQLConnection(postgresqlConnection);
    const brands: QueryResult<Brand> = await connection.query("SELECT * FROM brand");
    return brands.rows
  },
  getAllBrandDesc: async (): Promise<Brand[] | undefined> => {
    const connection = await createPostgreSQLConnection(postgresqlConnection);
    const brands: QueryResult<Brand> = await connection.query("SELECT * FROM brand ORDER BY name DESC");
    return brands.rows
  }, 
  getAllBrandAsc: async (): Promise<Brand[] | undefined> => {
    const connection = await createPostgreSQLConnection(postgresqlConnection);
    const brands: QueryResult<Brand> = await connection.query("SELECT * FROM brand ORDER BY name ASC");
    return brands.rows
  }
};