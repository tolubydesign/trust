import pg from "pg";

/**
 * A class to manage our connection to the postgresql 
 */
class PostgreSQLConnection {
  private pool: pg.Pool | undefined;
  constructor() {
    this.establishConnection();
  }

  private establishConnection() {
    this.pool = new pg.Pool({
      user: 'postgres',
      host: 'localhost',
      database: 'postgres',
      password: 'postgres',
      port: 5454,
    })

    console.log("Total connections: ", this.pool.totalCount);
    console.log("Awaiting connections: ", this.pool.waitingCount);
    console.log("Idle connections: ", this.pool.idleCount);
  }

  async getPostgreSQLConnection(): Promise<pg.PoolClient | undefined>  {
    if (!this.pool) return undefined
    const connection = await this.pool.connect();
    return connection;
  }
};

const postgresqlSingleton = new PostgreSQLConnection();
export default postgresqlSingleton.getPostgreSQLConnection();
