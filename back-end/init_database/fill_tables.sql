-- Filling brands
INSERT INTO brand 
	(name, url, alt)
VALUES 
	('del', 'http://location/image.jpg', 'Del logo.'),
	('honda', 'http://location/image.jpg', 'Honda logo.'),
	('lenovo legion', 'http://location/image.jpg', 'Lenovo Legion logo.'); 
