CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

DROP TABLE IF EXISTS brand;

-- Creation of brands table
CREATE TABLE IF NOT EXISTS brand (
  id uuid DEFAULT uuid_generate_v4 (),
  name VARCHAR(255) NOT NULL,
  alt VARCHAR NOT NULL,
  url TEXT
);
